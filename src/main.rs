use std::{
    io::{self, Write, BufReader, BufRead},
    mem,
    process::{self, Command, Stdio},
};
use eyre::{Result, WrapErr};
use regex::Regex;
use lazy_static::lazy_static;

struct Acc {
    diags: Vec<Vec<String>>,
    footer: Vec<String>,
    running: bool,
}

impl Acc {
    fn new() -> Acc {
        Acc {
            diags: Vec::new(),
            footer: Vec::new(),
            running: false,
        }
    }

    fn take_line(&mut self, line: String) {
        lazy_static! {
            static ref RE_FMT: Regex =
                Regex::new(r"\x1b\[[^m]{0,10}m").unwrap();
            static ref RE_CRATE: Regex =
                Regex::new(r"^   Compiling ").unwrap();
            static ref RE_DONE: Regex =
                Regex::new(r"^    Finished ").unwrap();
            static ref RE_DIAG: Regex =
                Regex::new(r"^(error|warning)").unwrap();
            static ref RE_FOOTER: Regex =
                Regex::new(r"^(error|warning).*(could not compile|generated .*? warning)").unwrap();
        };

        if self.running {
            self.emit(&line);
        }

        let stripped = RE_FMT.replace_all(&line, "");

        if RE_CRATE.is_match(&stripped) {
            self.flush();
            self.emit(&line);
        } else if RE_DONE.is_match(&stripped) {
            self.flush();
            self.emit(&line);
            self.running = true;
        } else if RE_FOOTER.is_match(&stripped) {
            self.footer.push(line);
        } else {
            if RE_DIAG.is_match(&stripped) || self.diags.is_empty() {
                self.diags.push(Vec::new());
            }
            self.diags.last_mut().unwrap().push(line);
        }
    }

    fn flush(&mut self) {
        let mut diags = mem::replace(&mut self.diags, Vec::new());
        for diag in diags.drain(..).rev() {
            for line in diag.into_iter() {
                self.emit(&line);
            }
        }
        self.diags = diags;
        let mut footer = mem::replace(&mut self.footer, Vec::new());
        for line in footer.drain(..) {
            self.emit(&line);
        }
        self.footer = footer;
    }

    fn emit(&self, line: &str) {
        eprint!("{}", line);
    }
}

fn main() -> Result<()> {
   let mut args = std::env::args();
   let _command = args.next();

   let mut cargo = Command::new("cargo")
       .args(args)
       .env("CARGO_TERM_COLOR", "always") // TODO: do own tty detection?
       .stderr(Stdio::piped())
       .spawn()
       .wrap_err("could not start cargo command")?;
    let mut output = BufReader::new(cargo.stderr.take().unwrap());
    let mut buf = String::new();
    let mut acc = Acc::new();
    loop {
        let n = output.read_line(&mut buf)
            .wrap_err("couldn't read cargo output")?;
        if n == 0 {
            acc.flush();
            break;
        }
        acc.take_line(mem::replace(&mut buf, String::new()));
        if acc.running {
            io::stderr().write_all(output.buffer())?;
            io::copy(&mut output.into_inner(), &mut io::stderr())?;
            break;
        }
    }

    let status = cargo.wait()?;
    process::exit(status.code().unwrap_or(-1));
}
