# `ograc`

ok, look. Imagine you have some not so correct code:

```rust
use std::env;
use std::meme;

fn main() {
    let cup_1 = Some(42);
    let cup_2 = None;
    let cup_3 = None;

    mem::replace(&mut cup_1, &mut cup_2);
    mem::replace(&mut cup_3, &mut cup_1);
    mem::replace(&mut cup_2, &mut cup_3);
    mem::replace(&mut cup_2, &mut cup_1);
    mem::replace(&mut cup_1, &mut cup_3);
    mem::replace(&mut cup_3, &mut cup_2);
    mem::replace(&mut cup_2, &mut cup_1);
    mem::replace(&mut cup_1, &mut cup_2);
    mem::replace(&mut cup_3, &mut cup_1);
    mem::replace(&mut cup_3, &mut cup_1);

    // ...
}
```

and you, a programmer of humble means, with a finitely sized terminal window,
asked `cargo` what's up:


```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ 🦞                                 ~/rip                               _ ☐ x ┃
┠──────────────────────────────────────────────────────────────────────────────┨
┃ $ cargo build▉                                                               ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

you'd get some not maximally helpful output.
```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ 🦞                                 ~/rip                               _ ☐ x ┃
┠──────────────────────────────────────────────────────────────────────────────┨
┃    |                                                                         ┃
┃ 14 |     mem::replace(&mut cup_3, &mut cup_2);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:15:5                                                       ┃
┃    |                                                                         ┃
┃ 15 |     mem::replace(&mut cup_2, &mut cup_1);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:16:5                                                       ┃
┃    |                                                                         ┃
┃ 16 |     mem::replace(&mut cup_1, &mut cup_2);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:17:5                                                       ┃
┃    |                                                                         ┃
┃ 17 |     mem::replace(&mut cup_3, &mut cup_1);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:18:5                                                       ┃
┃    |                                                                         ┃
┃ 18 |     mem::replace(&mut cup_3, &mut cup_1);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ warning: unused import: `std::env`                                           ┃
┃  --> src/main.rs:1:5                                                         ┃
┃   |                                                                          ┃
┃ 1 | use std::env;                                                            ┃
┃   |     ^^^^^^^^                                                             ┃
┃   |                                                                          ┃
┃   = note: `#[warn(unused_imports)]` on by default                            ┃
┃                                                                              ┃
┃ Some errors have detailed explanations: E0432, E0433.                        ┃
┃ For more information about an error, try `rustc --explain E0432`.            ┃
┃ warning: `rip` (bin "rip") generated 1 warning                               ┃
┃ error: could not compile `rip` due to 11 previous errors; 1 warning emitte   ┃
┃                                                                              ┃
┃ $ █                                                                          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

Like, the actual bad line of code isn't even shown. The most important error
message isn't visible anymore. Now, sure, you could scroll up or something,
but for those of us who need more than two attempts to get a program to
compile, that would get annoying fairly quickly.

There has to be a better way, right? That's why you'd instead use `ograc`.

```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ 🦞                                 ~/rip                               _ ☐ x ┃
┠──────────────────────────────────────────────────────────────────────────────┨
┃ $ cargo install ograc                                                        ┃
┃     Updating crates.io index                                                 ┃
┃   Downloaded ograc v0.1.0                                                    ┃
┃   Downloaded 1 crate (2.4 KB) in 0.95s                                       ┃
┃   Installing ograc v0.1.0                                                    ┃
┃    Compiling memchr v2.4.1                                                   ┃
┃    Compiling eyre v0.6.5                                                     ┃
┃    Compiling indenter v0.3.3                                                 ┃
┃    Compiling regex-syntax v0.6.25                                            ┃
┃    Compiling once_cell v1.8.0                                                ┃
┃    Compiling lazy_static v1.4.0                                              ┃
┃    Compiling aho-corasick v0.7.18                                            ┃
┃    Compiling regex v1.5.4                                                    ┃
┃    Compiling ograc v0.1.0                                                    ┃
┃     Finished release [optimized] target(s) in 7.68s                          ┃
┃   Installing /home/ben/.cargo/bin/ograc                                      ┃
┃    Installed package `ograc v0.1.0` (executable `ograc`)                     ┃
┃                                                                              ┃
┃ $ ograc build▉                                                               ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┃                                                                              ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

And voil&agrave;!

```
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ 🦞                                 ~/rip                               _ ☐ x ┃
┠──────────────────────────────────────────────────────────────────────────────┨
┃  --> src/main.rs:13:5                                                        ┃
┃    |                                                                         ┃
┃ 13 |     mem::replace(&mut cup_1, &mut cup_3);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:12:5                                                       ┃
┃    |                                                                         ┃
┃ 12 |     mem::replace(&mut cup_2, &mut cup_1);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:11:5                                                       ┃
┃    |                                                                         ┃
┃ 11 |     mem::replace(&mut cup_2, &mut cup_3);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃   --> src/main.rs:10:5                                                       ┃
┃    |                                                                         ┃
┃ 10 |     mem::replace(&mut cup_3, &mut cup_1);                               ┃
┃    |     ^^^ use of undeclared crate or module `mem`                         ┃
┃                                                                              ┃
┃ error[E0433]: failed to resolve: use of undeclared crate or module `mem`     ┃
┃  --> src/main.rs:9:5                                                         ┃
┃   |                                                                          ┃
┃ 9 |     mem::replace(&mut cup_1, &mut cup_2);                                ┃
┃   |     ^^^ use of undeclared crate or module `mem`                          ┃
┃                                                                              ┃
┃ error[E0432]: unresolved import `std::meme`                                  ┃
┃  --> src/main.rs:2:5                                                         ┃
┃   |                                                                          ┃
┃ 2 | use std::meme;                                                           ┃
┃   |     ^^^^^----                                                            ┃
┃   |     |    |                                                               ┃
┃   |     |    help: a similar name exists in the module: `mem`                ┃
┃   |     no `meme` in the root                                                ┃
┃                                                                              ┃
┃ warning: `rip` (bin "rip") generated 1 warning                               ┃
┃ error: could not compile `rip` due to 11 previous errors; 1 warning emitted  ┃
┃                                                                              ┃
┃  $ ▉                                                                         ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```

The important error is right there before your prompt, immediately visible, no
obstructions. It's that easy.

---

Thanks to [Cecile Tonglet](https://twitter.com/CecileTonglet) whose
[`cargo-firstpage`](https://github.com/cecton/cargo-firstpage) very directly
inspired and motivated this.
